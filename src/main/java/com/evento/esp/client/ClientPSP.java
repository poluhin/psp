package com.evento.esp.client;

import com.evento.esp.common.Crypter;
import com.evento.esp.common.crypter.BasicCrypter;
import lombok.var;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class ClientPSP {

    private Crypter crypter = new BasicCrypter();

    public <R> String encrypt(R data, Key key) {
        return crypter.encrypt(data, key);
    }

    public <R> String encryptRSA(R data, PublicKey key) {
        return crypter.encryptRSA(data, key);
    }

    public String decrypt(String data, Key key) {
        return crypter.decrypt(data, key);
    }

    public <R> R decrypt(String data, Key key, Class<R> clazz) {
        return crypter.decrypt(data, key, clazz);
    }

    public Key createKey() {
        return crypter.generateAESKey();
    }

    public PublicKey getPublicKeyFromFile(String filename) throws Exception {
        var keyBytes = Files.readAllBytes(Paths.get(filename));
        return getPublicKeyFromBytes(keyBytes);
    }

    public PublicKey getPublicKeyFromBytes(byte[] keyBytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
        var spec = new X509EncodedKeySpec(keyBytes);
        var keyFactory = KeyFactory.getInstance(BasicCrypter.RSA_ALGORITHM);
        return keyFactory.generatePublic(spec);
    }
}
