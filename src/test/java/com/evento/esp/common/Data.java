package com.evento.esp.common;

import lombok.AllArgsConstructor;

@lombok.Data
@AllArgsConstructor
public class Data {
    private String somethingData;
    private String anotherData;
}
