package com.evento.esp.common.jwt;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtToken {
    private String body;
    private String userId;
    private String hash;
    private String salt;
    private long expiration;

    public JwtToken(JwtToken jwtToken) {
        this.body = jwtToken.body;
        this.userId = jwtToken.userId;
        this.hash = jwtToken.hash;
        this.salt = jwtToken.salt;
        this.expiration = jwtToken.expiration;
    }
}
