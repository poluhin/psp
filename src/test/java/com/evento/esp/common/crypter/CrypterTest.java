package com.evento.esp.common.crypter;

import com.google.gson.Gson;
import com.evento.esp.common.Crypter;
import com.evento.esp.common.Data;
import com.evento.esp.common.jwt.JwtBuilder;
import com.evento.esp.common.jwt.JwtToken;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.SecretKeySpec;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CrypterTest {

    private Crypter crypter = new BasicCrypter();
    private JwtBuilder jwtBuilder = new JwtBuilder();

    @Test
    void crypterTest() {

        var data = new Data(RandomString.make(), RandomString.make());
        var jwtToken = jwtBuilder
                .setUserId(RandomString.make())
                .setExpiration(JwtBuilder.Expiration.TEST)
                .build(data);

        var key = crypter.generateAESKey();
        var encryptResult = crypter.encrypt(jwtToken, key);
        var decryptResult = crypter.decrypt(encryptResult, key, JwtToken.class);
        assertTrue(jwtBuilder.isValidToken(decryptResult));
    }

    @Test
    void crypterRsaTest() {
        var key = crypter.generateAESKey();

        var keyPair = crypter.generateKeyPair();
        var publicKey = keyPair.getPublic();
        var privateKey = keyPair.getPrivate();

        var data = jwtBuilder
                .setUserId(RandomString.make())
                .setExpiration(JwtBuilder.Expiration.TEST)
                .build(key);

        var encryptResult = crypter.encryptRSA(data, publicKey);
        var decryptResult = crypter.decryptRSA(encryptResult, privateKey, JwtToken.class);
        assertEquals(key, new Gson().fromJson(decryptResult.getBody(), SecretKeySpec.class));
    }
}
