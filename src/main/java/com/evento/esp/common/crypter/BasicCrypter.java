package com.evento.esp.common.crypter;

import com.google.gson.Gson;
import com.evento.esp.common.Crypter;
import lombok.var;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

public class BasicCrypter implements Crypter {

    private static final String ALGORITHM = "AES";
    private static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    public static final String RSA_ALGORITHM = "RSA";

    private static final Cipher cipher;
    private static final Cipher cipherRsa;
    private static final KeyGenerator keyGenerator;
    private static final KeyPairGenerator keyRsaGenerator;
    private static final Gson gson;

    static {
        try {
            gson = new Gson();
            cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipherRsa = Cipher.getInstance(RSA_ALGORITHM);
            keyGenerator = KeyGenerator.getInstance(ALGORITHM);
            keyGenerator.init(256);
            keyRsaGenerator = KeyPairGenerator.getInstance(RSA_ALGORITHM);
            keyRsaGenerator.initialize(4096);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public String encrypt(String data, Key key) {
        return encryptOrDecrypt(Cipher.ENCRYPT_MODE, cipher, key, data);
    }

    @Override
    public <R> String encrypt(R data, Key key) {
        var jsonData = gson.toJson(data);
        return encryptOrDecrypt(Cipher.ENCRYPT_MODE, cipher, key, jsonData);
    }

    @Override
    public String encryptRSA(String data, PublicKey key) {
        return encryptOrDecrypt(Cipher.ENCRYPT_MODE, cipherRsa, key, data);
    }

    @Override
    public <R> String encryptRSA(R data, PublicKey key) {
        var jsonData = gson.toJson(data);
        return encryptOrDecrypt(Cipher.ENCRYPT_MODE, cipherRsa, key, jsonData);
    }

    @Override
    public String decrypt(String data, Key key) {
        return encryptOrDecrypt(Cipher.DECRYPT_MODE, cipher, key, data);
    }

    @Override
    public <R> R decrypt(String data, Key key, Class<R> clazz) {
        return gson.fromJson(encryptOrDecrypt(Cipher.DECRYPT_MODE, cipher, key, data), clazz);
    }

    @Override
    public String decryptRSA(String data, PrivateKey key) {
        return encryptOrDecrypt(Cipher.DECRYPT_MODE, cipherRsa, key, data);
    }

    @Override
    public <R> R decryptRSA(String data, PrivateKey key, Class<R> clazz) {
        return gson.fromJson(encryptOrDecrypt(Cipher.DECRYPT_MODE, cipherRsa, key, data), clazz);
    }

    private String encryptOrDecrypt(int cipherMode, Cipher cipher, Key key, String data) {
        try {
            cipher.init(cipherMode, key);
            if (cipherMode == Cipher.ENCRYPT_MODE) {
                var cipherResult = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
                return Base64.getEncoder().encodeToString(cipherResult);
            } else {
                var cipherResult = cipher.doFinal(Base64.getDecoder().decode(data));
                return new String(cipherResult, StandardCharsets.UTF_8);
            }
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Key generateAESKey() {
        var key = keyGenerator.generateKey().getEncoded();
        try {
            var sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            return new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public KeyPair generateKeyPair() {
        return keyRsaGenerator.generateKeyPair();
    }
}