import com.evento.esp.client.ClientPSP;
import com.evento.esp.common.jwt.JwtBuilder;
import com.evento.esp.common.jwt.JwtToken;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import com.evento.esp.server.ServerESP;

import javax.crypto.spec.SecretKeySpec;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClientServerTest {

    private JwtBuilder jwtBuilder = new JwtBuilder();


    @Data
    @AllArgsConstructor
    private class AuthDataRequest {
        private String phone;
        private SecretKeySpec key;
    }

    @Data
    @AllArgsConstructor
    private class AuthResponseData {
        private String sessionToken;
    }

    @Test
    void authTest() {

        var client = new ClientPSP();
        var server = new ServerESP();
        /*
            Client use com.evento.esp.server public key and send phone + generated AES key
            Just for example, phone number -- random string

            In real case com.evento.esp.client should to get the com.evento.esp.server public key use a method getPublicKeyFromFile(String filename)
         */
        var keyPair = server.createKeyPair();
        var clientKey = client.createKey();

        var data = new AuthDataRequest(RandomString.make(), (SecretKeySpec) clientKey);
        var sendData = client.encryptRSA(data, keyPair.getPublic());
        /*
            Server part. For get the private key com.evento.esp.server should use a method getPrivateKeyFromFile(String filename)
            and the public key by a method getPublicKeyFromFile(String filename)
         */
        var decryptedData = server.decryptRSA(sendData, keyPair.getPrivate(), AuthDataRequest.class);
        assertEquals(clientKey, decryptedData.key);
        // Server create small expiration JWT Token to complete authorization
        // and encrypt by the com.evento.esp.server public key
        var jwtToken = server.createAuthJwtToken((SecretKeySpec) clientKey);
        var encryptedJwtToken = server.encryptRSA(jwtToken, keyPair.getPublic());
        // For sure it should be a data class
        var serverAuthResponse = new AuthResponseData(encryptedJwtToken);
        var encryptedAuthServerResponse = server.encrypt(serverAuthResponse, clientKey);
        /*
            Client part. Client will get SMS code from... Srly, from SMS.
         */
        var decryptedServerResponse = client.decrypt(encryptedAuthServerResponse, clientKey, AuthResponseData.class);
        assertEquals(serverAuthResponse, decryptedServerResponse);
        /*
            For next request com.evento.esp.server create a constant JWT token and save the hash to database.
            For authentification requests, com.evento.esp.server create a temporary token.
            Verification is check a token hash and a token expiration.
            For get new a session token, com.evento.esp.client should send a current constant JWT token and generate new key
            encrypted by a com.evento.esp.server public key.
         */
        var decryptedJwtToken = server.decryptRSA(decryptedServerResponse.sessionToken, keyPair.getPrivate(), JwtToken.class);
        assertTrue(jwtBuilder.isValidToken(decryptedJwtToken));
    }

}
