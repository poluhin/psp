package com.evento.esp.common.jwt;

import com.evento.esp.common.Data;
import lombok.var;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Random;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class JwtTokenTest {

    private JwtBuilder jwtBuilder = new JwtBuilder();
    private Random random = new Random();

    @Test
    void jwtBuilderTest() throws InterruptedException {
        var data = new Data(RandomString.make(), RandomString.make());
        var userId = RandomString.make();
        var jwtToken = jwtBuilder
                .setUserId(userId)
                .setExpiration(JwtBuilder.Expiration.TEST)
                .build(data);
        var copyJwtToken = new JwtToken(jwtToken);
        assertTrue(jwtBuilder.isValidToken(copyJwtToken));
        copyJwtToken.setUserId(RandomString.make());
        assertFalse(jwtBuilder.isValidToken(copyJwtToken));
        copyJwtToken.setUserId(userId);
        assertTrue(jwtBuilder.isValidToken(jwtToken));
        sleep((copyJwtToken.getExpiration() - Instant.now().getEpochSecond() + 1) * 1000); // epoch second to milliseconds
        assertFalse(jwtBuilder.isValidToken(copyJwtToken));
    }

}
