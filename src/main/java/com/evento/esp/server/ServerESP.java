package com.evento.esp.server;

import com.evento.esp.common.Crypter;
import com.evento.esp.common.crypter.BasicCrypter;
import com.evento.esp.common.jwt.JwtBuilder;
import com.evento.esp.common.jwt.JwtToken;
import lombok.var;

import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class ServerESP {

    private Crypter crypter = new BasicCrypter();
    private JwtBuilder jwtBuilder = new JwtBuilder();

    public KeyPair createKeyPair() {
        return crypter.generateKeyPair();
    }

    public <R> String encrypt(R data, Key key) {
        return crypter.encrypt(data, key);
    }

    public <R> String encryptRSA(R data, PublicKey publicKey) {
        return crypter.encryptRSA(data, publicKey);
    }

    public <R> R decryptRSA(String data, PrivateKey privateKey, Class<R> clazz) {
        return crypter.decryptRSA(data, privateKey, clazz);
    }

    public void saveKeyToFile(String path, Key key) {
        var file = new File(path);
        file.getParentFile().mkdirs();
        var strKey = key.getEncoded();
        try {
            var fos = new FileOutputStream(file);
            fos.write(strKey);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static PrivateKey getPrivateKeyFromFile(String filename) {
        try {
            var keyBytes = Files.readAllBytes(Paths.get(filename));
            var spec = new PKCS8EncodedKeySpec(keyBytes);
            var keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static PublicKey getPubicKeyFromFile(String filename) {
        try {
            var keyBytes = Files.readAllBytes(Paths.get(filename));
            var spec = new X509EncodedKeySpec(keyBytes);
            var keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(spec);
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public JwtToken createAuthJwtToken(SecretKeySpec key) {
        return jwtBuilder.setExpiration(JwtBuilder.Expiration.AUTH).build(key);
    }
}
