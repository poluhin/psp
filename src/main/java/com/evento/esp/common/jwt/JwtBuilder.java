package com.evento.esp.common.jwt;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.var;
import net.bytebuddy.utility.RandomString;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Random;

public class JwtBuilder {


    public enum Expiration {
        TEST, // 2 - 5 seconds
        AUTH, // 1 - 2 minutes
        NORMAL, // 1 - 10 minutes
        OVER // 1 - 6 hours
    }

    private static final Gson gson = new GsonBuilder().create();

    private static final Random random = new Random();
    private String userId;
    private Expiration expiration;

    public <R> JwtToken build(R data) {
        var token = new JwtToken();
        var jsonData = gson.toJson(data);
        token.setBody(jsonData);
        token.setExpiration(createExpiration());
        token.setUserId(userId);
        token.setSalt(RandomString.make());
        token.setHash(generateHash(token));
        return token;
    }

    public boolean isValidToken(JwtToken jwtToken) {
        var hash = generateHash(jwtToken);
        var isValidHash = hash.equals(jwtToken.getHash());
        var currentTime = Instant.now().getEpochSecond();
        var isValidExpiration = jwtToken.getExpiration() > currentTime;
        return isValidHash && isValidExpiration;
    }

    private String generateHash(JwtToken jwtToken) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
        var input = String.format("%s.%s.%s.%s",
                jwtToken.getBody(), jwtToken.getUserId(), jwtToken.getExpiration(), jwtToken.getSalt());
        var messageDigest = md.digest(input.getBytes());
        var signum = new BigInteger(1, messageDigest);
        var hash = new StringBuilder(signum.toString(16));
        while (hash.length() < 64) {
            hash.insert(0, random.nextInt());
        }
        return hash.toString();
    }

    public JwtBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JwtBuilder setExpiration(Expiration expiration) {
        this.expiration = expiration;
        return this;
    }

    private long createExpiration() {
        if (expiration == null) expiration = Expiration.NORMAL;
        var currentTime = Instant.now().getEpochSecond();
        // Time between 1 and 10 minutes
        long tokenExpiration;
        switch (expiration) {
            case TEST:
                tokenExpiration = random.nextInt(2) + 3; // between 2 and 5 second
                break;
            case AUTH:
                tokenExpiration = random.nextInt(60) + 60; // between 1 and 2 minutes
                break;
            case OVER:
                tokenExpiration = random.nextInt(60 * 60) + (60 * 60 * 5); // between 1 and 6 hours
                break;
            default:
                tokenExpiration = random.nextInt(60) + (60 * 9); // between 1 and 10 minutes
                break;
        }
        return currentTime + tokenExpiration;
    }
}
