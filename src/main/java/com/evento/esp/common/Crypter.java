package com.evento.esp.common;

import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public interface Crypter {

    String encrypt(String data, Key key);

    <R> String encrypt(R data, Key key);

    String encryptRSA(String data, PublicKey key);

    <R> String encryptRSA(R data, PublicKey key);

    String decrypt(String data, Key key);

    <R> R decrypt(String data, Key key, Class<R> clazz);

    String decryptRSA(String data, PrivateKey key);

    <R> R decryptRSA(String data, PrivateKey key, Class<R> clazz);

    Key generateAESKey();

    KeyPair generateKeyPair();
}
